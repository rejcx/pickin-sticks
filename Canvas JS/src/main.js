$( document ).ready( function() {
	var settings = { width: 640, height: 480, fps: 30 };
	var images = {};
	var sounds = {};
	var keys = { up: 38, down: 40, left: 37, right: 39 };
	$( "canvas" ).attr( "width", settings.width );
	$( "canvas" ).attr( "height", settings.height );
	
	var canvasWindow = $("canvas")[0].getContext( "2d" );
	
	// Loading Screen
	canvasWindow.fillStyle = "#666666";
	canvasWindow.fillRect( 0, 0, settings.width, settings.height );		
	canvasWindow.fillStyle = "#FFFFFF";
	canvasWindow.fillText( "Loading...", 10, 10 );
	
	window.addEventListener( "keydown", HandleInput, false );
	
	objGirl = { x: 320, y: 320, width:32, height: 48, speed: 5, score: 0 };
	objStick = { x: 0, y: 0, width: 32, height: 32 };
	
	Setup();

	function Setup()
	{
		images.girl = new Image();
		images.girl.src = "assets/Delphine_Still.png";
		
		images.grass = new Image();
		images.grass.src = "assets/Grass.png";
		
		images.sticks = new Image();
		images.sticks.src = "assets/Sticks.png";
		
		sounds.success = new Audio();
		sounds.success.src = "assets/success.ogg";
		
		GenerateStickCoordinates();
	
		setInterval( function() {
			Update();
			Draw();
		}, 1000 / settings.fps );	
	}
	
	function Update()
	{
		if ( 	objGirl.x < objStick.x + objStick.width &&
				objGirl.x + objGirl.width > objStick.x &&
				objGirl.y < objStick.y + objStick.height &&
				objGirl.y + objGirl.height > objStick.y )
		{
			// Collision
			GenerateStickCoordinates();
			objGirl.score += 1;
			sounds.success.play();	
		}
	}
	
	function Draw()
	{
		DrawBackground();
		DrawEntities();
		DrawHUD();
	}

	function HandleInput( ev )
	{
		// Arrow keys: UP(38) DOWN(40) LEFT(37) RIGHT(39)
		if ( ev.keyCode == keys.up )
		{
			objGirl.y -= objGirl.speed;
		}
		else if ( ev.keyCode == keys.down )
		{
			objGirl.y += objGirl.speed;
		}
		if ( ev.keyCode == keys.left )
		{
			objGirl.x -= objGirl.speed;
		}
		else if ( ev.keyCode == keys.right )
		{
			objGirl.x += objGirl.speed;
		}
	}
	
	/* Game Code */
	function DrawBackground()
	{
		for ( var y = 0; y < settings.height/64; y++ )
		{
			for ( var x = 0; x < settings.width/64; x++ )
			{
				canvasWindow.drawImage( images.grass, x*64, y*64 );
			}
		}
	}
	
	function DrawEntities()
	{		
		// Source x, y, w, h - Dest x, y, w, h
		canvasWindow.drawImage( images.sticks, 0, 0, 32, 32, objStick.x, objStick.y, objStick.width, objStick.height );
		
		canvasWindow.drawImage( images.girl, objGirl.x, objGirl.y );
	}
	
	function DrawHUD()
	{
		canvasWindow.fillStyle = "#FFFFFF";
		canvasWindow.font = "20px Arial";
		canvasWindow.fillText( "Score: " + objGirl.score, 15, 30 );
	}
	
	function GenerateStickCoordinates()
	{
		objStick.x = Math.floor( Math.random() * settings.width );
		objStick.y = Math.floor( Math.random() * settings.height );
	}

} );

