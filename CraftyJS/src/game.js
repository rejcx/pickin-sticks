Game = {

	width: function() { return 320; },
	height: function() { return 480; },

	start: function()
	{
		Crafty.init( 320, 240 );
		Crafty.background( "rgb( 100, 100, 100 )" );
		Crafty.scene( "Loading" );
	}

}

Crafty.scene( "Loading", function() 
{
	// Loading text
	Crafty.e( "2D, DOM, Text" )
		.text( "Loading..." )
		.attr( { x: 0, y: 0 } )
		
	// Load assets
	Crafty.load( [
		"assets/Delphine_Anim.png",
		"assets/Grass.png"
	] );
	
	Crafty.scene( "Game" );
} );

Crafty.scene( "Game", function()
{
	Crafty.background('rgb(255, 200, 200)');
	
	var sprGrass = Crafty.sprite( 1, "assets/Grass.png", 
		{ grass: [0, 0, 64, 64] } );
	
	var entGrass = Crafty.e( "Grass" );
} );

Crafty.c( "Grass", {
	init: function() 
	{
		this.addComponent( "2D, Canvas, Color, grass" );
	}
} );




