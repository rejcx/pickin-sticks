import pygame

class Application:
	def __init__(self):
		pygame.init()
		self.done = False
		self.fpsClock = pygame.time.Clock()
		
		self.screenDimen = ( 640, 480 )
		
		self.window = pygame.display.set_mode( self.screenDimen )
		pygame.display.set_caption( "PyGame Pickin' Sticks" )
	
	def Update( self ):
		self.fpsClock.tick( 30 )
	
	def DrawScreen( self ):
		pygame.display.update()
		
	def DrawText( self, surfaceMessage, rectRegion ):
		self.window.blit( surfaceMessage, rectRegion )
		
	def DrawEntity( self, entity, image ):
		self.window.blit( image, entity.GetCoordinates(), entity.GetFrame() )
		
	def DrawTile( self, image, coordinates, frameRect ):
		self.window.blit( image, coordinates, frameRect )
		
	def QuitProgram( self ):
		return self.done
