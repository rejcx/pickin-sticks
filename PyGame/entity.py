import random
import pygame

class Entity:
	def __init__(self):
		self.x = self.y = 0
		self.frameX = 0
		self.frameY = 0
		
	def Setup( self, x, y, w, h ):
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		
	def GetCoordinates( self ):
		return self.x, self.y
		
	def GetFrame( self ):
		frameRect = pygame.Rect( int(self.frameX) * self.w, int(self.frameY) * self.h, self.w, self.h )
		return frameRect
		
	def Update( self ):
		self.frameX += 0.1
		if ( self.frameX >= 3 ):
			self.frameX = 0
	
	# x, y, w, h

class Player( Entity ):
	def Setup( self, x, y, w, h, speed ):
		Entity.Setup( self, x, y, w, h )
		self.speed = speed
		self.score = 0
	
	def GetSpeed( self ):
		return self.speed
		
	def GetScore( self ):
		return self.score
		
	def IncrementScore( self ):
		self.score += 1	
	
	def Move( self, keys ):
		if keys[pygame.K_LEFT]:
			self.MoveLeft()
		elif keys[pygame.K_RIGHT]:
			self.MoveRight()
		
		if keys[pygame.K_UP]:
			self.MoveUp()
		elif keys[pygame.K_DOWN]:
			self.MoveDown()
		
		self.Update()
	
	def MoveUp( self ):
		self.y -= self.speed
		self.frameY = 1
	def MoveDown( self ):
		self.y += self.speed
		self.frameY = 0
	def MoveLeft( self ):
		self.x -= self.speed
		self.frameY = 2
	def MoveRight( self ):
		self.x += self.speed	
		self.frameY = 3
	
class Stick( Entity ):
	def Setup( self, w, h ):
		Entity.Setup( self, 0, 0, w, h )
		self.frameX = 2
		self.frameY = 0
		self.GenerateNewCoordinates()
	
	def GenerateNewCoordinates( self ):
		self.x = random.randint( 0, 640 - self.w )
		self.y = random.randint( 0, 480 - self.h )
