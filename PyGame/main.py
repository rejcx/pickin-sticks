import pygame, sys
import random
from entity import Entity
from entity import Player
from entity import Stick
from application import Application

app = Application()

player = Player()
player.Setup( 640/2, 480/2, 32, 48, 5 )

stick = Stick()
stick.Setup( 32, 32 )

# This can still be cleaned up a lot!
colWhite = pygame.Color( 255, 255, 255 )
fntMain = pygame.font.Font( "../_font/Averia-Bold.ttf", 20 )
imgPlayer = pygame.image.load( "../_gfx/AyneAlpha.png" )
imgTileset = pygame.image.load( "../_gfx/TilesetAlpha.png" )

while not app.QuitProgram():
	# -- Update
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			sys.exit()

	keys = pygame.key.get_pressed()
	player.Move( keys )
	
	# End for event in pygame.event.get():
	
	if ( player.x < stick.x + stick.w and player.x + player.w > stick.x 
	and player.y < stick.y + stick.h and player.y + player.h > stick.y ):
		player.IncrementScore()
		stick.GenerateNewCoordinates()
	
	# -- Draw		
	for y in range( 0, 480/32 ):
		for x in range( 0, 640/32 ):
			app.DrawTile( imgTileset, ( x * 32, y * 32 ), pygame.Rect( 0, 0, 32, 32 ) )
	
	app.DrawEntity( player, imgPlayer )
	app.DrawEntity( stick, imgTileset )
	
	txtScore = "Score: " + str( player.GetScore() )
	surfMessage = fntMain.render( txtScore, False, colWhite )
	rectMessage = surfMessage.get_rect()
	app.DrawText( surfMessage, rectMessage )
	
	app.DrawScreen()
	app.Update()
	
	
	
	
 
