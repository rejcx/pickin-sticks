/* SDL Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#include "Application.h"
#include "ImageManager.h"

#include <iostream>
#include "SDL/SDL.h"

int main()
{
    Application application;
    ImageManager imageManager;

    while ( !application.QuitProgram() )
    {
        application.Update();

        // Draw grass tile
        for ( int x = 0; x < 640 / 32; x++ )
        {
            for ( int y = 0; y < 480 / 32; y++ )
            {
                // Legal C, legal C++ but damn ugly and breaks if SDL_Rect is updated.
                SDL_Rect texturePosition = { 0, 0, 32, 32 };

                SDL_Rect screenPosition;
                screenPosition.x = x * 32;
                screenPosition.y = y * 32;

                SDL_BlitSurface( imageManager.environmentTileset,
                                &texturePosition,
                                application.Buffer(),
                                &screenPosition
                                );
            }
        }

        // Draw player
        SDL_Rect texturePosition;
        texturePosition.x = texturePosition.y = 0;
        texturePosition.w = 32;
        texturePosition.h = 48;

        SDL_Rect screenPosition;
        screenPosition.x = 640/2;
        screenPosition.y = 480/2;

        SDL_BlitSurface( imageManager.characterAyne,
                        &texturePosition,
                        application.Buffer(),
                        &screenPosition
                        );

        application.FlipScreen();
    }

    return 0;
}
