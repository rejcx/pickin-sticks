/* SDL Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#ifndef _APPLICATION
#define _APPLICATION

#include "SDL/SDL.h"

#include "Timer.h"

/*
Application Class -
Takes care of basic Allegro functionality
*/
class Application
{
    public:
    Application();
    ~Application();

    void Update();
    void ClearScreen();
    void FlipScreen();
    SDL_Surface* Buffer();

    bool QuitProgram();

    protected:
    SDL_Surface* m_display;
    Timer m_timer;

    bool m_quitProgram;

    // Configuration Settings
    float m_FPS;
    SDL_Rect m_screenDimen;
};

#endif
