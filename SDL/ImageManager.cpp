/* SDL Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#include "ImageManager.h"

#include <iostream>

// TODO: If you wrap the image objs in a class, they can take care of bad loads on their own!
ImageManager::ImageManager()
{
    SDL_Surface* tempAyne = SDL_LoadBMP( "../_gfx/AyneMagenta.bmp" );
    if ( tempAyne == NULL )
    {
        std::cout << "Error loading image AyneMagenta.bmp" << std::endl;
    }
    characterAyne = SDL_DisplayFormat( tempAyne );
    SDL_FreeSurface( tempAyne );
    Uint32 transparentColor = SDL_MapRGB( characterAyne->format, 0xFF, 0, 0xFF );

    SDL_Surface* tempTileset = SDL_LoadBMP( "../_gfx/TilesetMagenta.bmp" );
    if ( tempTileset == NULL )
    {
        std::cout << "Error loading image TilesetMagenta.bmp" << std::endl;
    }
    environmentTileset = SDL_DisplayFormat( tempTileset );
    SDL_FreeSurface( tempTileset );

    // Turn magenta transparent
    SDL_SetColorKey( characterAyne, SDL_SRCCOLORKEY, transparentColor );
    SDL_SetColorKey( environmentTileset, SDL_SRCCOLORKEY, transparentColor );
}

ImageManager::~ImageManager()
{
    SDL_FreeSurface( characterAyne );
    SDL_FreeSurface( environmentTileset );
}
