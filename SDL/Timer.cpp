/* SDL Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#include "Timer.h"

#include "SDL/SDL.h"

Timer::Timer()
{
}

void Timer::Start()
{
    m_zeroTicks = SDL_GetTicks();
}

int Timer::GetElapsedTime()
{
    return SDL_GetTicks() - m_zeroTicks;
}
