/* SDL Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#include "Application.h"

#include <iostream>

// TODO: Cleanup init stuff, is oogly.
Application::Application()
{
    m_screenDimen.w = 640;
    m_screenDimen.h = 480;
    m_FPS = 60;
    m_quitProgram = false;

    if ( !SDL_Init( SDL_INIT_EVERYTHING ) )
    {
        std::cerr << "Error on SDL_Init(...)" << std::endl;
    }

    m_display = SDL_SetVideoMode( m_screenDimen.w, m_screenDimen.h, 32, SDL_SWSURFACE );
    if ( m_display == NULL )
    {
        std::cerr << "Error on SDL_SetVideoMode(...)" << std::endl;
    }

    SDL_WM_SetCaption( "Pickin' Sticks SDL 2012", NULL );
}

Application::~Application()
{
    SDL_FreeSurface( m_display );
    SDL_Quit();
}

bool Application::QuitProgram()
{
    return m_quitProgram;
}

SDL_Surface* Application::Buffer()
{
    return m_display;
}

void Application::Update()
{
    m_timer.Start();
    SDL_Event event;
    while ( SDL_PollEvent( &event ) )
    {
        if ( event.type == SDL_QUIT )
        {
            m_quitProgram = true;
        }
    }

    if ( m_timer.GetElapsedTime() < 1000 / m_FPS )
    {
        SDL_Delay( ( 1000 / m_FPS ) - m_timer.GetElapsedTime() );
    }

    ClearScreen();
}

void Application::ClearScreen()
{
    SDL_FillRect( m_display, NULL, SDL_MapRGB( m_display->format, 100, 100, 100 ) );
}

void Application::FlipScreen()
{
    SDL_Flip( m_display );
}
