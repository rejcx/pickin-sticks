/* SDL Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#ifndef _IMAGEMANAGER
#define _IMAGEMANAGER

#include "SDL/SDL.h"

// Lazy image manager
class ImageManager
{
    public:
    ImageManager();
    ~ImageManager();

    SDL_Surface* characterAyne;
    SDL_Surface* environmentTileset;
};

#endif
