/* SDL Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#ifndef _TIMER
#define _TIMER

class Timer
{
    public:
    Timer();
    void Start();
    int GetElapsedTime();

    protected:
    int m_zeroTicks;
};

#endif
