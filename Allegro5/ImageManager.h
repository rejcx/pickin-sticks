/* Allegro5 Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#ifndef _IMAGEMANAGER
#define _IMAGEMANAGER

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

// Lazy image manager
class ImageManager
{
    public:
    ImageManager();
    ~ImageManager();

    ALLEGRO_BITMAP* characterAyne;
    ALLEGRO_BITMAP* environmentTileset;
};

#endif
