#include "ImageManager.h"

#include <iostream>

// TODO: If you wrap the image objs in a class, they can take care of bad loads on their own!
ImageManager::ImageManager()
{
    characterAyne = al_load_bitmap( "../_gfx/AyneMagenta.bmp" );
    if ( characterAyne == NULL )
    {
        std::cout << "Error loading image AyneMagenta.bmp" << std::endl;
    }

    environmentTileset = al_load_bitmap( "../_gfx/TilesetMagenta.bmp" );
    if ( environmentTileset == NULL )
    {
        std::cout << "Error loading image TilesetMagenta.bmp" << std::endl;
    }

    al_convert_mask_to_alpha( characterAyne, al_map_rgb( 255, 0, 255 ) ); // Turn magetna transparent
}

ImageManager::~ImageManager()
{
    al_destroy_bitmap( characterAyne );
    al_destroy_bitmap( environmentTileset );
}
