/* Allegro5 Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#include "Application.h"
#include "ImageManager.h"

#include <iostream>

int main()
{
    Application application;
    ImageManager imageManager;

    while ( !application.QuitProgram() )
    {
        application.Update();

        // Draw grass tile
        for ( int x = 0; x < 640 / 32; x++ )
        {
            for ( int y = 0; y < 480 / 32; y++ )
            {
                al_draw_bitmap_region( imageManager.environmentTileset, 0, 0, 32, 32, x*32, y*32, NULL );
            }
        }

        // Draw player
        al_draw_bitmap_region( imageManager.characterAyne, 0, 0, 32, 48, 640/2, 480/2, NULL );

        application.FlipScreen();
    }

    return 0;
}
