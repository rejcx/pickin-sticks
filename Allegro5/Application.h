/* Allegro5 Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#ifndef _APPLICATION
#define _APPLICATION

#include <allegro5/allegro.h>

#include "CoordDimen.h"

/*
Application Class -
Takes care of basic Allegro functionality
*/
class Application
{
    public:
    Application();
    ~Application();

    void Update();
    void ClearScreen();
    void FlipScreen();

    bool QuitProgram();

    protected:
    ALLEGRO_DISPLAY* m_display;
    ALLEGRO_EVENT_QUEUE* m_eventQueue;
    ALLEGRO_TIMER* m_timer;

    bool m_quitProgram;
    bool m_refreshScreen;

    // Configuration Settings
    float m_FPS;
    Dimen m_screenDimen;
};

#endif
