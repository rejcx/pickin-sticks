/* Allegro5 Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#ifndef _COORDDIMEN
#define _COORDDIMEN

class Coord
{
    public:
    float x, y;

    Coord() { ; }

    Coord( float x, float y )
    {
        this->x = x;
        this->y = y;
    }

    Coord& operator=( const Coord& rhs )
    {
        x = rhs.x;
        y = rhs.y;
        return *this;
    }
};

class Dimen
{
    public:
    float w, h;

    Dimen() { ; }

    Dimen( float w, float h )
    {
        this->w = w;
        this->h = h;
    }

    Dimen& operator=( const Dimen& rhs )
    {
        w = rhs.w;
        h = rhs.h;
        return *this;
    }
};

#endif
