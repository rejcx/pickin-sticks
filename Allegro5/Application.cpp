/* Allegro5 Pickin' Sticks by Rachel J. Morris
Moosader.com, github.com/moosader.  zlib license */

#include "Application.h"

#include <allegro5/allegro_image.h>

#include <iostream>

// TODO: Cleanup init stuff, is oogly.
Application::Application()
{
    m_screenDimen = Dimen( 640, 480 );
    m_FPS = 60;
    m_quitProgram = false;
    m_refreshScreen = false;

    if ( !al_init() )
    {
        std::cerr << "Error on al_init()" << std::endl;
    }

    if ( !al_init_image_addon() )
    {
        std::cerr << "Error on al_init_image_addon()" << std::endl;
    }

    m_display = al_create_display( m_screenDimen.w, m_screenDimen.h );
    if ( m_display == NULL )
    {
        std::cerr << "Error on al_create_display(...)" << std::endl;
    }

    m_timer = al_create_timer( 1.0 / m_FPS );
    if ( m_timer == NULL )
    {
        std::cerr << "Error on al_create_timer(...)" << std::endl;
    }

    m_eventQueue = al_create_event_queue();
    if ( m_eventQueue == NULL )
    {
        std::cerr << "Error on al_create_event_queue()" << std::endl;
    }

    al_register_event_source( m_eventQueue, al_get_display_event_source( m_display ) );
    al_register_event_source( m_eventQueue, al_get_timer_event_source( m_timer ) );

    al_start_timer( m_timer );
}

Application::~Application()
{
    al_destroy_timer( m_timer );
    al_destroy_event_queue( m_eventQueue );
    al_destroy_display( m_display );
}

bool Application::QuitProgram()
{
    return m_quitProgram;
}

void Application::Update()
{
    ALLEGRO_EVENT event;
    al_wait_for_event( m_eventQueue, &event );

    if ( event.type == ALLEGRO_EVENT_DISPLAY_CLOSE )
    {
        m_quitProgram = true;
    }
    else if ( event.type == ALLEGRO_EVENT_TIMER && al_is_event_queue_empty( m_eventQueue ) )
    {
        m_refreshScreen = true;
    }

    ClearScreen();
}

void Application::ClearScreen()
{
    al_clear_to_color( al_map_rgb( 100, 100, 100 ) );
}

void Application::FlipScreen()
{
    if ( m_refreshScreen )
    {
        al_flip_display();
        m_refreshScreen = false;
    }
}
